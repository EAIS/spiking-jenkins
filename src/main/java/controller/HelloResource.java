package controller;

import model.HelloWorld;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class HelloResource {
    private static final String template = "Hello, %s";
    private final AtomicLong count = new AtomicLong();

    @RequestMapping("greeting")
    public HelloWorld greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new HelloWorld(count.incrementAndGet(), String.format(template, name));
    }
}
