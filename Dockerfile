FROM java:8
EXPOSE 9090
ADD /build/libs/eais-data-center-0.0.1.jar eais-data-center.jar
ENTRYPOINT ["java", "-jar", "eais-data-center.jar", "--server.port=9090"]